with DAG('ecommerce_etl', schedule_interval='@daily') as dag:

  t1 = PostgresToRedshiftOperator(...)  
  t2 = MongoDBToRedshiftOperator(...)

  t3 = TransformRedshiftOperator(...)

  t4 = LoadToStagingRedshiftOperator(...)
  t5 = InsertToAnalyticsRedshiftOperator(...)

  t6 = CalculateMetricsRedshiftOperator(...)

  t1 >> t2 >> t3 >> t4 >> t5 >> t6
