# Liquidpay_interview
For this test you will create data and domain models for a simple e-commerce application that caters for buying products online.
The system shall hold information about:

    Products
    Orders
    Payments
    Users (both registered as well as anonymous guests)

The system will have full-text search capability to allow the user to search and each product should have the following properties.

    Name
    Description
    Rating
    Category
    Price

Once the application receives the list of products, the user will be able to place an order, the API will receive the following properties :

    Order ID (Generated backend side)
    List of Products
    Total Price
    Status (Open, Completed)

The system should also verify if the product exists or/and there are enough items on the stock.
The system will be based on Micro-Services and expose REST APIs to connect from frontend applications, such as web and mobile apps.
Please provide an entity domain model for the application with all relevant information in UML format including but not limited to the above-mentioned entities.
Please create a data model optimized for transactional throughput and performance.

    Create appropriate dml and ddl scripts for a SQL based data storage
    Suggest one no-sql based storage alternative. Give key reasons for your suggestion

Please suggest meaningful analytical measures for a business intelligence layer to be extracted from the data available.

    Suggest data storage concept and technologies
    Provide recommendation for data synchronization between transactional and analytical data or alternatively suggest better suitable architecture to enable reporting and business intelligence

The results of your considerations should be summarized in a short presentation (format of your choice) and where possible executable code is preferred.