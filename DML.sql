INSERT INTO Products (id, name, description, rating, category, price)
VALUES (1, 'Product 1', 'Description 1', 4.5, 'Category 1', 9.99);

INSERT INTO Orders (id, user_id, total_price, status, placed_at)
VALUES (1, 123, 19.98, 'Open', '2023-02-14 12:34:56');

INSERT INTO OrderItems (id, order_id, product_id, quantity)
VALUES (1, 1, 1, 2); 

INSERT INTO Users (id, name, email, password) 
VALUES (123, 'Ha Hoang', 'HaH123@gmail.com', 'awD&1Gdy@1k9%!');

INSERT INTO Payments (id, order_id, amount, payment_method, paid_at)
VALUES (1, 1, 19.98, 'Credit Card', '2023-02-14 12:35:06');