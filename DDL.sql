CREATE TABLE Products (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  description TEXT,
  rating DECIMAL(2,1),
  category VARCHAR(255),
  price DECIMAL(10,2)
);

CREATE TABLE Orders (
  id INT PRIMARY KEY,
  user_id INT,
  total_price DECIMAL(10,2),
  status VARCHAR(20),
  placed_at TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES Users(id)
);

CREATE TABLE OrderItems (
  id INT PRIMARY KEY,
  order_id INT,
  product_id INT,
  quantity INT,
  FOREIGN KEY (order_id) REFERENCES Orders(id),
  FOREIGN KEY (product_id) REFERENCES Products(id)
);

CREATE TABLE Users (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255),
  password VARCHAR(255)
);

CREATE TABLE Payments (
  id INT PRIMARY KEY,
  order_id INT,
  amount DECIMAL(10,2),
  payment_method VARCHAR(50),
  payment_date TIMESTAMP,
  FOREIGN KEY (order_id) REFERENCES Orders(id)
);
